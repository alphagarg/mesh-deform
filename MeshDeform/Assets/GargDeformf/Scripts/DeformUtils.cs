﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace GargDeformf
{
	class BoolPtr
	{
		public bool value;
	}
	public class MeshDeformHelper
	{
		public Transform filterTransform;
		public Mesh mesh;
		public Vector3[] vertices;
		public Vector3[] ogVertices;
		public MeshDeformHelper(MeshFilter mf)
		{
			filterTransform = mf.transform;
			
			mesh = mf.mesh;
			mf.sharedMesh = mesh;
			
			vertices = new Vector3[mesh.vertexCount];
			ogVertices = new Vector3[mesh.vertexCount];
			
			System.Array.Copy(mesh.vertices, vertices, vertices.Length);
			System.Array.Copy(mesh.vertices, ogVertices, vertices.Length);
		}
		public void ResetVertices()
		{
			System.Array.Copy(ogVertices, vertices, vertices.Length);
			mesh.SetVertices(vertices);
		}
		public void DeformAndApply(Vector3 collisionPoint, Vector3 collisionImpulse, float bendDistance, float bendIntensity)
		{
			if (Deform(collisionPoint, collisionImpulse, bendDistance, bendIntensity))
				ApplyVertices();
		}
		public bool Deform(Vector3 collisionPoint, Vector3 collisionImpulse, float bendDistance, float bendIntensity)
		{
			collisionPoint = filterTransform.InverseTransformPoint(collisionPoint);
			collisionImpulse = filterTransform.InverseTransformDirection(collisionImpulse) * .01f;
			
			var deformed = new BoolPtr();
			D_0(collisionPoint, collisionImpulse, bendDistance, bendIntensity, deformed);
			
			return deformed.value;
		}
		async void D_0(Vector3 collisionPoint, Vector3 collisionImpulse, float bendDistance, float bendIntensity, BoolPtr deformed)
		{
			await Task.Run(() => D_1(collisionPoint, collisionImpulse, bendDistance, bendIntensity, deformed));
		}
		void D_1(Vector3 collisionPoint, Vector3 collisionImpulse, float bendDistance, float bendIntensity, BoolPtr deformed)
		{
			for (int i = vertices.Length - 1; i >= 0; i--)
			{
				float imp = Mathf.Clamp(1 * bendDistance - Vector3.Distance(collisionPoint, vertices[i]), 0, .75f * bendIntensity);
				if (imp > 0)
					vertices[i] += collisionImpulse * imp;
				deformed.value |= imp > 0;
			}
		}
		public void ApplyVertices()
		{
			mesh.SetVertices(vertices, 0, vertices.Length);
		}
	}
}