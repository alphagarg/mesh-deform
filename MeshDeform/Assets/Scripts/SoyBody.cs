﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GargDeformf
{
	public class SoyBody : MonoBehaviour
	{
		public float intensity = 1;
		public float softness = 1;
		MeshDeformHelper[] mdhs;
		
		public void ResetDamage()
		{
			foreach (var e in mdhs)
				e.ResetVertices();
		}
		
		void Start()
		{
			MeshFilter[] mf = transform.GetComponentsInChildren<MeshFilter>();
			mdhs = new MeshDeformHelper[mf.Length];
			for (int i = mf.Length - 1; i >= 0; i--)
				mdhs[i] = new MeshDeformHelper(mf[i]);
		}
		void OnCollisionEnter(Collision col)
		{
			var contacts = new ContactPoint[col.contactCount];
			col.GetContacts(contacts);
			
			foreach(ContactPoint cp in contacts)
				foreach (var e in mdhs)
					e.Deform(cp.point, col.relativeVelocity, softness, intensity);
			
			foreach (var e in mdhs)
				e.ApplyVertices();
		}
	}
}